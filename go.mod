module gitlab.com/wobcom/transceiver-exporter

go 1.13

require (
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.6.0
	github.com/prometheus/common v0.9.1
	gitlab.com/wobcom/ethtool v0.0.0-20200506142918-ebfbb8bf5250
)
